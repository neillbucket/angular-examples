import { Component, OnInit } from '@angular/core';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-fb',
  templateUrl: './fb.component.html',
  styleUrls: ['./fb.component.css']
})
export class FbComponent implements OnInit {
  id = 'mickeymouse'
  fbidUrl = 'https://graph.facebook.com/mickeymouse/picture?type=normal'

  constructor() { }

  ngOnInit() {
  }

  resolveImageURL(){
    this.fbidUrl =`https://graph.facebook.com/${this.id}/picture?type=normal`
  }
}
