import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //Styles is an array [] as you can have more than one style sheet 
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //Angular uses MVC - model = the project, view = html
  title = 'time for coffee';
  today = new Date()
  //declare some models for our data
  products = [{desc: 'Pots', price: 12.99},
  {desc: 'Dots', price: 3.99},
  {desc: 'Spots', price: 24.99}]

  flag = true;
  showFlag = false;
  myURL = 'https://via.placeholder.com/150';

  //declare listener functions
  handleClick(){
    console.log(event.target)
    this.flag = false;
    this.showFlag = true;
  }


}

